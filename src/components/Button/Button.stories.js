import { storiesOf } from "@storybook/vue";

import GinButton from "../Button/Button.vue";
import ButtonMD from "./Button.md";

storiesOf("Button", module)
  .addParameters({
    readme: {
      sidebar: ButtonMD
    }
  })
  .add("Basic", () => ({
    components: { GinButton },
    template:
      "<div> <gin-button type='default'>Default</gin-button> <gin-button type='primary'>Primary</gin-button> <gin-button type='secondary'>Secondary</gin-button> <gin-button type='info'>Info</gin-button> <gin-button type='success'>Success</gin-button> <gin-button type='danger'>Danger</gin-button> <gin-button type='warning'>Warning</gin-button></div>"
  }))
  .add("Outline", () => ({
    components: { GinButton },
    template:
      "<div> <gin-button outline type='default'>Default</gin-button> <gin-button outline type='primary'>Primary</gin-button> <gin-button outline type='secondary'>Secondary</gin-button> <gin-button outline type='info'>Info</gin-button> <gin-button outline type='success'>Success</gin-button> <gin-button outline type='danger'>Danger</gin-button> <gin-button outline type='warning'>Warning</gin-button></div>"
  }));
