## Button

### Usage
```html
 <gin-button>Default</gin-button>
```

### Prop Configuration

```html
 <gin-button type="primary" size="lg" icon="ni ni-atom" block outline disabled>Default</gin-button>
```

### Props
<table><thead><tr><th class="prop-col">Prop Name</th> <th class="type-col">Type</th> <th class="default-col">Default</th> <th class="description-col">Description</th></tr></thead> <tbody><tr><td>
        tag
      </td> <td>
        String
      </td> <td>
        button
      </td> <td>
        Button tag (default -&gt; button)
      </td></tr><tr><td>
        type
      </td> <td>
        String
      </td> <td>
        default
      </td> <td>
        Button type (default, primary, secondary, danger, info, success, warning)
      </td></tr><tr><td>
        size
      </td> <td>
        String
      </td> <td>
        N/A
      </td> <td>
        Button size lg|sm
      </td></tr><tr><td>
        textColor
      </td> <td>
        String
      </td> <td>
        N/A
      </td> <td>
        Button text color (e.g primary, danger etc)
      </td></tr><tr><td>
        nativeType
      </td> <td>
        String
      </td> <td>
        button
      </td> <td>
        Button native type (e.g submit,button etc)
      </td></tr><tr><td>
        icon
      </td> <td>
        String
      </td> <td>
        N/A
      </td> <td>
        Button icon
      </td></tr><tr><td>
        text
      </td> <td>
        String
      </td> <td>
        N/A
      </td> <td>
        Button text in case not provided via default slot
      </td></tr><tr><td>
        outline
      </td> <td>
        Boolean
      </td> <td>
        N/A
      </td> <td>
        Whether button style is outline
      </td></tr><tr><td>
        rounded
      </td> <td>
        Boolean
      </td> <td>
        N/A
      </td> <td>
        Whether button style is rounded
      </td></tr><tr><td>
        iconOnly
      </td> <td>
        Boolean
      </td> <td>
        N/A
      </td> <td>
        Whether button contains only an icon
      </td></tr><tr><td>
        block
      </td> <td>
        Boolean
      </td> <td>
        N/A
      </td> <td>
        Whether button is of block type
      </td></tr></tbody></table>
    