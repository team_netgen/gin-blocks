## Input

### Usage
```html
 <gin-input>Default</gin-input>
```

### Props
<table><thead><tr><th class="prop-col">Prop Name</th> <th class="type-col">Type</th> <th class="default-col">Default</th> <th class="description-col">Description</th></tr></thead> <tbody><tr><td>
        required
      </td> <td>
        Boolean
      </td> <td>
        N/A
      </td> <td>
        Whether input is required (adds an asterix *)
      </td></tr><tr><td>
        valid
      </td> <td>
        Boolean
      </td> <td>
        N/A
      </td> <td>
        Whether is valid
      </td></tr><tr><td>
        label
      </td> <td>
        String
      </td> <td>
        N/A
      </td> <td>
        Input label (text before input)
      </td></tr><tr><td>
        error
      </td> <td>
        String
      </td> <td>
        N/A
      </td> <td>
        Input error (below input)
      </td></tr><tr><td>
        labelClasses
      </td> <td>
        String
      </td> <td>
        N/A
      </td> <td>
        Input label css classes
      </td></tr><tr><td>
        inputClasses
      </td> <td>
        String
      </td> <td>
        N/A
      </td> <td>
        Input css classes
      </td></tr><tr><td>
        value
      </td> <td>
        String | Number
      </td> <td>
        N/A
      </td> <td>
        Input value
      </td></tr><tr><td>
        addonRightIcon
      </td> <td>
        String
      </td> <td>
        N/A
      </td> <td>
        Addon right icon
      </td></tr><tr><td>
        addonLeftIcon
      </td> <td>
        String
      </td> <td>
        N/A
      </td> <td>
        Addont left icon
      </td></tr></tbody></table>



      