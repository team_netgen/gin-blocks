import { storiesOf } from "@storybook/vue";
import { withKnobs, text } from "@storybook/addon-knobs";

import GinInput from "../Input/Input.vue";
import InputMD from "./Input.md";

storiesOf("Input", module)
  .addDecorator(withKnobs)
  .addParameters({
    readme: {
      sidebar: InputMD
    }
  })
  .add("Basic", () => ({
    components: { GinInput },
    props: {
      placeholder: {
        default: text("Placeholder", "test placeholder")
      }
    },
    template: "<gin-input :placeholder='placeholder'></gin-input>"
  }))
  .add("Validation", () => ({
    components: { GinInput },
    template:
      "<div class='row'> <div class='col-md-6'> <gin-input placeholder='Success' :valid='true'></gin-input> </div><div class='col-md-6'> <gin-input placeholder='Error input' :valid='false'></gin-input> </div></div>"
  }));
