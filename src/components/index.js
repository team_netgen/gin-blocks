import Vue from "vue";
import GinButton from "./Button/Button.vue";
import GinInput from "./Input/Input.vue";

const Components = {
  GinButton,
  GinInput
};

Object.keys(Components).forEach(name => {
  Vue.component(name, Components[name]);
});

export default Components;
