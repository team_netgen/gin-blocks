# Gin Blocks

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Lints and fixes files
```
npm run lint
```

## Adding Components

- Components should be adder under src/components in their own folder
- They need to have a corresponding .stories.js file for storybook and a markdown file explaing its usage and props
- The component mus also be added to the array in the src/components/index.js file 

## Publishing

Step 1: 
```
npm version minor --force
```

Step 2:
Commit your changes and push to origin 