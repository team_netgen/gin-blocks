import { configure, addParameters, addDecorator } from '@storybook/vue';
import { addReadme } from 'storybook-readme/vue';


addParameters({
    options: {
      name: 'Gin Blocks',
      isFullscreen: false,
      showPanel: true,
      // more configuration here
    },
    readme: {
        // You can set the global code theme here.
        codeTheme: 'github',
      },
  });

  addDecorator(addReadme);

// automatically import all files ending in *.stories.js
configure(require.context('../src', true, /\.stories\.js$/), module);